Repo
====

Bash scripts to work with submodules on a git master repo.

Assumes the presence of two local branches:

- deployed
- a branch with the project name

Usage
-----


Print help

	$ repo

Print status of the submodules

	$ repo check

Checkout a branch of all the master repositories' submodules


	$ repo checkout <branchname>

Create a branch <branchname> for every submodule


	$ repo create <branchname>

Merge a <branchname> onto an **existing** branch deployed

	$ repo finish <branchname>

Open an instance of gitx --all for each submodule

	$ repo gitx


